FROM nginx:1.23.0-alpine
COPY /nginx-conf/nginx.conf /etc/nginx/conf.d/
COPY /nginx-conf/tls-nginx.conf /etc/nginx/conf.d/
COPY example.ubuntu.com /etc/nginx/sites-available
RUN ln -s /etc/nginx/sites-available /etc/nginx/sites-enabled
RUN mkdir -p /home/site/app1
RUN	mkdir -p /home/site/app2
RUN	echo "APPLICATION 1" > /home/site/app1/index.html
RUN	echo "APPLICATION 2" > /home/site/app2/index.html

CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
FROM python

CMD ["python3", "-m", "http.server", "-d", "/home/site/app1/index.html" ]
CMD ["python3", "-m", "http.server", "-d", "/home/site/app2/index.html" ]



