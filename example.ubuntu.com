    upstream myapp1 {
        server localhost:85;
        server localhost:86;
	server localhost:87;

#	sticky;
#	ip_hash;    
#	sticky cookie srv_id expires=1h domain=example.ubuntu.com path=/;
}


server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;

        server_name example.ubuntu.com;
        root /var/www/tutorial;
        index index.html;

#location of tsl keys
        ssl_certificate /etc/nginx/live/example.ubuntu.com/fullchain.pem;
        ssl_certificate_key /etc/nginx/live/example.ubuntu.com/privkey.pem;

#location of tsl config
	include /etc/nginx/conf.d/tsl-nginx.conf;


       location / {
               try_files $uri $uri/ =404;

       }

	location /app1 {
		proxy_pass http://localhost:85/;

	}


	location /lb {
		proxy_pass http://myapp1/;

	}

}

server {
       listen 80;
       listen [::]:80;

       server_name example.ubuntu.com;

	
       root /var/www/tutorial;
       index index.html;

       location / {
               try_files $uri $uri/ =404;

       }

	location /app1 {
		proxy_pass http://localhost:85/;

}


	location /lb {
		proxy_pass http://myapp1/;

}
#password login
       location /p {
                root /var/www/tutorial;

               auth_basic "Restricted Content";
               auth_basic_user_file /etc/nginx/.htpasswd;
       }



}
